import firebase from 'firebase';
import firebaseConfig from '../../config/firebase';
if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
    // firebase.app()
}

const Notas = {
    listNotas: (uid,cbf) => {
        firebase.database().ref(`Users/${uid}/Notas`)
        .on('value',data =>cbf(data))
        
    },
    addNota: (uid,Nota) => {
        firebase.database().ref(`Users/${uid}/Notas`).push(Nota,()=>{
            console.log('Complite')
        })
    },
    updateNota: async (uid,nid, Nota) => {
        await firebase.database().ref(`Users/${uid}/Notas/${nid}`).update(Nota)
    },
    deleteNota: async (uid,nid) => {
        const resp = await firebase.database().ref(`Users/${uid}/Notas/${nid}`).remove()
        return resp;
    }
}
export default Notas