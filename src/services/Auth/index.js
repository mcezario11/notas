import firebase from 'firebase';
import firebaseConfig from '../../config/firebase';


if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
    // firebase.app()
}


const Auth = {
    cadastrarUser: async (Nome, email, password) => {
        try {
            let resp = await firebase.auth().createUserWithEmailAndPassword(email, password)   
            const id = resp.user.uid;
            await firebase.database().ref(`Users/${id}`).set({ Nome })
            return {status: 'ok', user: resp.user}
        } catch (error) {
            return {status: 'erro', msg: error}
        }

         
    },
    login: async (email, password) => {
        try {
            let resp = await firebase.auth().signInWithEmailAndPassword(email, password);
            return {status: 'ok', user: resp.user}
        } catch (error) {
            return {status: 'erro', msg: error}
        }
    },
    getUserName:  (uid,cbf) => {
         firebase.database().ref(`Users/${uid}/Nome`)
        .once('value',(data) =>cbf(data))
    }
}
export default Auth;