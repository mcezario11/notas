import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Login from './pages/Login';
import Cadastro from './pages/Cadastro';
import Home from './pages/Home';
import AddOrUpdateNote from './pages/AddOrUpdateNote';

import { StatusBar } from 'react-native'
StatusBar.setBackgroundColor('#fff');
StatusBar.setBarStyle('dark-content');
const Router = createAppContainer(
    createStackNavigator(
        {
            Login: {
                screen: Login
            },
            Cadastro: {
                screen: Cadastro
            },
            Home:Home,
            AddOrUpdateNote: {
                screen: AddOrUpdateNote
            }
        },
        {
            initialRouteName: 'Login',
            headerLayoutPreset: 'center',
            defaultNavigationOptions: {
                title: 'Notas',
            }
        }
    )
);
export default Router;