import React, { useState } from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { CheckBox, Button, Input } from 'react-native-elements'
import Auth from '../../services/Auth';

export default function Cadastro({navigation}) {
    const [visibility, setVisibility] = useState(false);
    const [nome, setNome] = useState('');
    const [email, setEmail] = useState('');
    const [senha, setSenha] = useState('');
    const inputs = {email:null,senha:null}
    return (
        <View >
         <View style={styles.input}>
                <Input
                    value={nome}
                    onChangeText={(nome)=>setNome(nome)}
                    placeholder='Nome completo'
                    onSubmitEditing={()=>{
                        inputs.email.focus();
                    }}
                    leftIcon={
                        <Icon
                            name='face'
                            size={24}
                            color='grey'
                        />
                    }
                />
            </View>
            <View style={styles.input}>
                <Input
                    value={email}
                    onChangeText={(email)=>setEmail(email)}
                    keyboardType="email-address"
                    placeholder='email@exemplo.com'
                    onSubmitEditing={()=>{
                        inputs.senha.focus();
                    }}
                    ref={ref => {
                    inputs.email = ref;
                    }}
                    leftIcon={
                        <Icon
                            name='email'
                            size={24}
                            color='grey'
                        />
                    }
                />
            </View>

            <View style={styles.input}>
                <Input
                    value={senha}
                    onChangeText={(senha)=>setSenha(senha)}
                    secureTextEntry={!visibility}
                    placeholder='Senha'
                    ref={ref => {
                    inputs.senha = ref;
                    }}
                    leftIcon={
                        <Icon
                            name={visibility ? 'lock-open' : 'lock'}
                            size={24}
                            color='grey'
                        />
                    }
                />
            </View>

            <View style={styles.button}>
                <CheckBox

                    center
                    title='Mostrar senha'
                    checked={visibility}
                    onPress={() => setVisibility((v) => !v)}
                    iconType='material'
                    checkedIcon='visibility'
                    uncheckedIcon='visibility-off'
                />
            </View>

        
            <View style={styles.button}>
                <Button
                    title="Cadastrar"
                    type="outline"
                    onPress={async ()=>{
                        let resp = await Auth.cadastrarUser(nome,email,senha)
                        if(resp.status === 'ok'){
                            Alert.alert('Cadastro feito com sucesso!');
                            navigation.navigate('Home',{
                                user: resp.user
                            })
                        } else {
                            Alert.alert(''+resp.msg);
                        }
                    }}

                />
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    button: {
        marginHorizontal: 20,
        marginVertical: 10
    },
    input: {
        marginHorizontal: 20,
        marginVertical: 10
    }
});