import React, { useState } from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { CheckBox, Button, Input } from 'react-native-elements'
import Auth from '../../services/Auth';

// import { Container } from './styles';

export default function Login({navigation}) {

    const [visibility, setVisibility] = useState(false);
    const [email, setEmail] = useState('');
    const [senha, setSenha] = useState('');
    const inputs = {senha:null}
    const _login = async () => {
        let resp = await Auth.login(email,senha);
        if(resp.status === 'ok'){

             navigation.navigate('Home',{
                user: resp.user
            })
        } else {
            Alert.alert(''+resp.msg);
        }
    }
    return (
        <View >
            <View style={styles.input}>
                <Input
                    keyboardType="email-address"
                    placeholder='email@exemplo.com'
                    value={email}
                    onChangeText={(email)=>setEmail(email)}
                    onSubmitEditing={()=>{
                        inputs.senha.focus();
                    }}
                    leftIcon={
                        <Icon
                            name='email'
                            size={24}
                            color='grey'
                        />
                    }
                />
            </View>

            <View style={styles.input}>
                <Input
                    secureTextEntry={!visibility}
                    placeholder='Senha'
                    ref={(ref)=>{inputs.senha=ref}}
                    onSubmitEditing={()=>{
                        _login()
                    }}
                    value={senha}
                    onChangeText={(senha)=>setSenha(senha)}
                    leftIcon={
                        <Icon
                            name={visibility ? 'lock-open' : 'lock'}
                            size={24}
                            color='grey'
                        />
                    }
                />
            </View>

            <View style={styles.button}>
                <CheckBox

                    center
                    title='Mostrar senha'
                    checked={visibility}
                    onPress={() => setVisibility((v) => !v)}
                    iconType='material'
                    checkedIcon='visibility'
                    uncheckedIcon='visibility-off'
                />
            </View>

            <View style={styles.button}>
                <Button
                    title="Login"
                    type="outline"
                    onPress={ () => {_login()}}
                />
            </View>

            <View style={styles.button}>
                <Button
                    title="Cadastro"
                    type="outline"
                    onPress={()=>navigation.navigate('Cadastro')}

                />
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    button: {
        marginHorizontal: 20,
        marginVertical: 10
    },
    input: {
        marginHorizontal: 20,
        marginVertical: 10
    }
});