import React, {useState, useEffect} from 'react';
import { View, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { CheckBox, Button, Input } from 'react-native-elements'
import { withNavigationFocus } from 'react-navigation';
import Notas from '../../services/Notas';
// import { Container } from './styles';

function AddOrUpdateNote({navigation, isFocused}) {
    const [state, setState] = useState({title:'',description:''});
    let texto = null;
    useEffect(()=>{
        if(isFocused) {
            const {userId, note} = navigation.getParam('body');
            if(note){
                setState({title: note.title, description: note.description})
            }
        }
    },[isFocused])
    const _save = () => {
        const {userId, note} = navigation.getParam('body');
        if(note){
            Notas.updateNota(userId,note.id,state);
        } else {
            Notas.addNota(userId,state);
        }
        navigation.goBack();
    }
    return (
        <View >
            <View style={styles.input}>
                <Input
                    value={state.title}
                    onSubmitEditing={()=>{
                        texto.focus();
                    }}
                    onChangeText={(text)=>{
                        setState((prevState)=>({...prevState,title: text}))}}
                    placeholder='Titulo'
                />
            </View>
            <View style={styles.input}>
                <Input
                    value={state.description}
                    ref={(ref)=>{texto=ref}}
                    
                    onChangeText={(text)=>{
                        setState((prevState)=>({...prevState,description:text}))}}
                    placeholder='Descrição'
                    multiline
                />
            </View>
            <View style={styles.button}>
                <Button
                    title="Salvar"
                    type="outline"
                    onPress={_save}
                />
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    button: {
        marginHorizontal: 20,
        marginVertical: 10
    },
    input: {
        marginHorizontal: 20,
        marginVertical: 10
    }
});

export default withNavigationFocus(AddOrUpdateNote);