import React, {useEffect, useState} from 'react';
import { View,Text, FlatList, StyleSheet, Alert } from 'react-native';
import { Button, SearchBar } from 'react-native-elements';
import Item from '../../components/Item';
import Notas from '../../services/Notas';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {YellowBox} from 'react-native';
import EmptyItem from '../../components/EmptyItem';
import Auth from '../../services/Auth';

export default function Home({navigation}) {
    const [data,setData] = useState([]);
    const [search, setSearch] = useState('');
    const [refreshing, setRefreshing] = useState(false);
    const user = navigation.getParam('user');
    const _updateNote = async () => {
        await Notas.listNotas(user.uid,d=>{
            const data2 = d.toJSON();
            const dataList = []
            for(let i in data2){
                data2[i].id = i;
                dataList.push(data2[i])
            }
            setData(dataList);
            
        })
    }
    useEffect(()=>{
        Auth.getUserName(user.uid, (d)=>{
            Home.navigationOptions = () =>( {
                title: 'Olá '+d.node_.value_
            })
        })
        YellowBox.ignoreWarnings(['Setting a timer'])
        _updateNote();
        
    },[])
    const _listEmptyComponent = () => {
        return (
            <View>
                <EmptyItem/>
            </View>
        )
    }
    const getData = () => {
        const dataList = data.filter(
            d => d.title.toLowerCase().startsWith(search.toLowerCase())
            );
        return dataList
    }
    const _refresh = async () => {
        setRefreshing(true);
        await _updateNote();
        setRefreshing(false)
    }
    return (
        <View style={{flex:1}} >
        <SearchBar
        placeholder="Buscar"
        platform="android"
        onChangeText={setSearch}
        value={search}
        
       
      />
        <FlatList
                data={getData()}
                onRefresh={_refresh}
                refreshing={refreshing}
                ListEmptyComponent={_listEmptyComponent}
                keyExtractor={item => item.id}
                renderItem={({item}) => <Item 
                title={item.title} 
                description={item.description}
                id={item.id}
                onDelete={async (id)=>{
                    try{
                        await Notas.deleteNota(user.uid,id)
                        Alert.alert("Deletado com sucesso!")
                    }catch(e){
                        Alert.alert("Erro ao deletar")
                    }
                    
                }}
                onEdit={(id)=>{
                    const note = data.find(i=> i.id === id);
                    navigation.navigate('AddOrUpdateNote',{
                    body:{
                        userId:user.uid,
                        note,
                    }
                });
                }}
                 />}
            />
            <Button 
           
            onPress={()=>{
                navigation.navigate('AddOrUpdateNote',{
                    body:{
                        userId:user.uid
                    }
                });
            }}
            buttonStyle={styles.botao}
            icon={ <Icon  name="add" size={50} color="#fff" />}
            />
        </View>
    );
}
const styles = StyleSheet.create({
    botao: {
      position: 'absolute',
      right: 10,
      bottom:10,
      margin:10,
    
      width: 80,
      height: 80,
     
      borderRadius: 50,
      justifyContent: 'center',
      alignItems: 'center',
      zIndex: 3
    },
    
})