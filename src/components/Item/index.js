import React from 'react';
import { Text, StyleSheet, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Card, Button } from 'react-native-elements'
// import { Container } from './styles';

export default function Item({title,description,id,onEdit,onDelete}) {
  return (
    <Card
  title={title}>
  <Text style={{marginBottom: 10}}>
  {
      description
  }
  </Text>
  
    <Button
      icon={<Icon name='mode-edit' color='#ffffff' size={22} />}
      buttonStyle={styles.button}
      onPress={()=>{onEdit(id)}}
      
      title='Editar' />
    <Button
      icon={<Icon name='delete' color='#ffffff' size={22} />}
      buttonStyle={[styles.button,{backgroundColor: '#d00'}]}
      
      onPress={()=>{onDelete(id)}}
      title='Deletar' />
  
  
</Card>
  );
}

const styles = StyleSheet.create({
    button:{
        borderRadius: 15,
        height: 30,
        margin:10
        
       
        
    }
});
