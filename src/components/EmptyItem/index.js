import React from 'react';
import { View,Text } from 'react-native';
import { Card } from 'react-native-elements';
import LottieView from 'lottie-react-native';
import notFound from '../../../assets/animations/notFound.json'
// import { Container } from './styles';

export default function EmptyItem() {
  return (
    <Card title="OPS!!!" >
    <Text style={{marginBottom: 10, fontSize:24}}>
      Não encontramos nada aqui!
    </Text>
    <LottieView source={notFound} autoPlay loop style={{
      position:'relative'
      
    }} />
      
    
    
  </Card>
  );
}
